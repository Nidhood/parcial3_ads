package com.javeriana.nominiest.model;

public class Employees {

    // Attributes:

    private String name;
    private String document_id;
    private String company_dependency;
    private String company_charge;
    private int minimum_wages_number;
    private double current_legal_minimum_wage;

    // Constructors:

    public Employees() {
    }
    public Employees(String name, String document_id, String company_dependency, String company_charge, int minimum_wages_number, double current_legal_minimum_wage) {
        this.name = name;
        this.document_id = document_id;
        this.company_dependency = company_dependency;
        this.company_charge = company_charge;
        this.minimum_wages_number = minimum_wages_number;
        this.current_legal_minimum_wage = current_legal_minimum_wage;
    }


    // Getters and setters:

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDocument_id() {
        return document_id;
    }
    public void setDocument_id(String document_id) {
        this.document_id = document_id;
    }
    public String getCompany_dependency() {
        return company_dependency;
    }
    public void setCompany_dependency(String company_dependency) {
        this.company_dependency = company_dependency;
    }
    public String getCompany_charge() {
        return company_charge;
    }
    public void setCompany_charge(String company_charge) {
        this.company_charge = company_charge;
    }
    public int getMinimum_wages_number() {
        return minimum_wages_number;
    }
    public void setMinimum_wages_number(int minimum_wages_number) {
        this.minimum_wages_number = minimum_wages_number;
    }
    public double getCurrent_legal_minimum_wage() {
        return current_legal_minimum_wage;
    }
    public void setCurrent_legal_minimum_wage(double current_legal_minimum_wage) {
        this.current_legal_minimum_wage = current_legal_minimum_wage;
    }

    // Methods:

    public void Add_subject(){}
    public void Add_teacher(){}
    public void Add_monitor(){}
    public double Calculate_the_salary_of_an_teacher() {
        int total_salary = 0;
        return total_salary;
    }
    public double Calculate_the_salary_of_an_monitor() {
        int total_salary = 0;
        return total_salary;
    }

}
