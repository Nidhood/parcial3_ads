package com.javeriana.nominiest.model;

import java.util.ArrayList;
import java.util.List;

public class Monitors extends Employees{

    // Attributes:

    private double value_per_hour_worked;
    private List<Subjects> subjectsList = new ArrayList<>();

    // Constructors:

    public Monitors() {
    }
    public Monitors(double value_per_hour_worked, List<Subjects> subjectsList) {
        this.value_per_hour_worked = value_per_hour_worked;
        this.subjectsList = subjectsList;
    }
    public Monitors(String name, String document_id, String company_dependency, String company_charge, int minimum_wages_number, double current_legal_minimum_wage, double value_per_hour_worked, List<Subjects> subjectsList) {
        super(name, document_id, company_dependency, company_charge, minimum_wages_number, current_legal_minimum_wage);
        this.value_per_hour_worked = value_per_hour_worked;
        this.subjectsList = subjectsList;
    }

    // Getters and setters:

    public double getValue_per_hour_worked() {
        return value_per_hour_worked;
    }
    public void setValue_per_hour_worked(double value_per_hour_worked) {
        this.value_per_hour_worked = value_per_hour_worked;
    }
    public List<Subjects> getSubjectsList() {
        return subjectsList;
    }
    public void setSubjectsList(List<Subjects> subjectsList) {
        this.subjectsList = subjectsList;
    }

    // Methods:

    public void Add_employee(){}

}
