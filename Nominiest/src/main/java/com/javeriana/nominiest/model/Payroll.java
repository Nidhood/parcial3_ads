package com.javeriana.nominiest.model;

import java.util.ArrayList;
import java.util.List;

public class Payroll {

    // Attributes:

    private List<Employees> employeesList = new ArrayList<>();

    // Constructors:

    public Payroll() {
    }
    public Payroll(List<Employees> employeesList) {
        this.employeesList = employeesList;
    }

    // Getters and setters:

    public List<Employees> getEmployeesList() {
        return employeesList;
    }
    public void setEmployeesList(List<Employees> employeesList) {
        this.employeesList = employeesList;
    }

    // Methods:

    public void Add_employee(){}
    public double Calculate_the_salary_of_an_employee() {
        int total_salary = 0;
        return total_salary;
    }
    public void generate_file(){}
}
