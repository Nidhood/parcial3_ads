package com.javeriana.nominiest.model;

public class Subjects {
    // Attributes:

    private String name;
    private int number_of_hours;

    // Constructors:

    public Subjects() {
    }
    public Subjects(String name, int number_of_hours) {
        this.name = name;
        this.number_of_hours = number_of_hours;
    }

    // Getters and setters:

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getNumber_of_hours() {
        return number_of_hours;
    }
    public void setNumber_of_hours(int number_of_hours) {
        this.number_of_hours = number_of_hours;
    }

    // Methods:
}
