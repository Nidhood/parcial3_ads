package com.javeriana.nominiest.model;

import java.util.ArrayList;
import java.util.List;

public class Teachers extends Employees{

    // Attributes:

    private String [] promotion_ladder;
    private double value_per_hour_worked;
    private List<Subjects> subjectsList = new ArrayList<>();

    // Constructors:

    public Teachers() {
    }
    public Teachers(String[] promotion_ladder, double value_per_hour_worked, List<Subjects> subjectsList) {
        this.promotion_ladder = promotion_ladder;
        this.value_per_hour_worked = value_per_hour_worked;
        this.subjectsList = subjectsList;
    }
    public Teachers(String name, String document_id, String company_dependency, String company_charge, int minimum_wages_number, double current_legal_minimum_wage, String[] promotion_ladder, double value_per_hour_worked, List<Subjects> subjectsList) {
        super(name, document_id, company_dependency, company_charge, minimum_wages_number, current_legal_minimum_wage);
        this.promotion_ladder = promotion_ladder;
        this.value_per_hour_worked = value_per_hour_worked;
        this.subjectsList = subjectsList;
    }

    // Getters and setters:

    public String[] getPromotion_ladder() {
        return promotion_ladder;
    }
    public void setPromotion_ladder(String[] promotion_ladder) {
        this.promotion_ladder = promotion_ladder;
    }
    public double getValue_per_hour_worked() {
        return value_per_hour_worked;
    }
    public void setValue_per_hour_worked(double value_per_hour_worked) {
        this.value_per_hour_worked = value_per_hour_worked;
    }
    public List<Subjects> getSubjectsList() {
        return subjectsList;
    }
    public void setSubjectsList(List<Subjects> subjectsList) {
        this.subjectsList = subjectsList;
    }

    // Methods:

    public void Add_teacher_subjects(){}

}
