module com.javeriana.nominiest {
    requires javafx.controls;
    requires javafx.fxml;
    opens com.javeriana.nominiest.controller to javafx.fxml;
    exports com.javeriana.nominiest.controller;
}